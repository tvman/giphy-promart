<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class GiphyController extends Controller
{
  public function index()
  {
    return view('index');
  }

  public function upload()
  {
    return view('upload');
  }

  public function login()
  {
    return view('login');
  }

  public function trending()
  {
    return view('trending_all');
  }

}
