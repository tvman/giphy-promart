<section id="search-results" class="is-hidden">
  <header>
    <h2>Results for "@{{ keyword }}"</h2>
    <h4 class="see-all">Total Count: @{{ totalCount }}</a>
  </header>
  <ul>
    <li v-if="searchs" v-for="gif in searchs">
      <img v-lazy="gif.images.original.webp">
    </li>
  </ul>
</section>
