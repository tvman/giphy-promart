<section id="trending">
  <header>
    <h2>Trending Gifs</h2>
    <a href="{{ route('trending') }}" class="see-all">see all <i class="icon-right"></i></a>
  </header>
  <ul>
    <li v-if="trendings" v-for="gif in trendings">
      <img v-lazy="gif.images.original.webp">
    </li>
  </ul>
</section>
