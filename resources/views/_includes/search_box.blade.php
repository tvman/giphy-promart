<div id="search">
  <div class="container">
    <form action="">
      <div class="text-animations">
        <div class="wrap-text-animations">
          <p>@username + tag to search within a verified channel</p>
          <p>Search all the GIFs and Stickers</p>
          <p>@username + tag to search within a verified channel</p>
        </div>
      </div>
      <input class="search-input" type="text" name="keyword" v-model="keyword" @keyup="getSearchGifs">
      <a href="#" class="search-button icon-search"></a>
      </button>
    </form>
  </div>
</div>
