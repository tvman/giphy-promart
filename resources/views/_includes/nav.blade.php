<nav id="nav">
  <div class="container">
    <a href="{{ route('index') }}" class="logo">
      <div class="logo-svg">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 28 35" itemProp="logo">
          <g fill-rule="evenodd" clip-rule="evenodd">
          <path fill="#00ff99" d="M0 3h4v29H0z"></path>
          <path fill="#9933ff" d="M24 11h4v21h-4z"></path>
          <path fill="#00ccff" d="M0 31h28v4H0z"></path>
          <path fill="#fff35c" d="M0 0h16v4H0z"></path>
          <path fill="#ff6666" d="M24 8V4h-4V0h-4v12h12V8"></path>
          <path class="shadow" d="M24 16v-4h4M16 0v4h-4"></path></g>
        </svg>
      </div>
      <header>
        <h1 class="logo-text">Giphy</h1>
      </header>
    </a>
    <ul class="links-mobile">
      <li class="link-cross">
         <a href="{{ route('upload') }}">
          <svg width="39" height="39" xmlns="http://www.w3.org/2000/svg">
            <defs><path d="M21.489 17.511V9.953H17.51v7.558H9.953v3.978h7.558v7.558h3.978v-7.558h7.558V17.51h-7.558zm9.027-1.469v6.916h-7.558v7.558h-6.916v-7.558H8.484v-6.916h7.558V8.484h6.916v7.558h7.558z" id="createiconpath"></path>
            </defs>
            <g fill="none" fill-rule="evenodd">
              <path d="M0 0h39v39H0z"></path>
              <use fill="#0F9" fill-rule="nonzero" xlink:href="#createiconpath"></use>
            </g>
          </svg>
         </a>
      </li>
      <li class="link-user">
        <a href="#">
          <svg width="39" height="39" xmlns="http://www.w3.org/2000/svg">
            <defs><path d="M23.143 23.443a9.696 9.696 0 0 1-4.31 1.002 9.686 9.686 0 0 1-4.548-1.124A7.87 7.87 0 0 0 10.8 29h15.65a7.868 7.868 0 0 0-3.307-5.557zM28 29.894v.606H9.25v-.606a9.374 9.374 0 0 1 5.04-8.315 8.152 8.152 0 0 0 4.543 1.366 8.16 8.16 0 0 0 4.354-1.243A9.372 9.372 0 0 1 28 29.894zM19.071 19.33c2.713 0 4.911-2.2 4.911-4.915a4.913 4.913 0 0 0-4.91-4.915 4.912 4.912 0 0 0-4.911 4.915 4.913 4.913 0 0 0 4.91 4.915zm0 1.5a6.413 6.413 0 0 1-6.41-6.415A6.413 6.413 0 0 1 19.07 8a6.413 6.413 0 0 1 6.411 6.415 6.413 6.413 0 0 1-6.41 6.415z" id="usericonpath"></path>
            </defs>
            <g fill="none" fill-rule="evenodd">
              <path d="M0 0h39v39H0z"></path>
              <use fill="#FFF35C" fill-rule="nonzero" xlink:href="#usericonpath"></use>
            </g>
          </svg>
        </a>
      </li>
      <li class="link-bar">
        <a href="#">
          <svg rotate="-90deg" width="39" height="39" xmlns="http://www.w3.org/2000/svg">
            <g fill="none" fill-rule="evenodd">
              <path d="M0 0h39v39H0z"></path>
              <path d="M12 12h-1v-2h19v2H12zm-4 9H7v-2h23v2H8zm4 8h-1v-2h19v2H12z" fill="#0CF"></path>
            </g>
          </svg>

          <svg class="bar-off" rotate="90deg" width="39" height="39" xmlns="http://www.w3.org/2000/svg">
            <g fill="none" fill-rule="evenodd">
              <path d="M0 0h39v39H0z"></path>
              <path fill="#0CF" d="M10.858 12.272l15.87 15.87.756.756 1.414-1.414-.756-.756-15.87-15.87-.756-.756-1.414 1.414z"></path>
              <path fill="#0CF" d="M28.142 12.272l-15.87 15.87-.756.756-1.414-1.414.756-.756 15.87-15.87.756-.756 1.414 1.414z"></path>
            </g>
          </svg>

        </a>
      </li>
    </ul>
    <div class="links-desktop">
      <ul class="links">
        <li class="link"><a href="#">Reactions</a></li>
        <li class="link"><a href="#">Entertainment</a></li>
        <li class="link"><a href="#">Sports</a></li>
        <li class="link"><a href="#">Stickers</a></li>
        <li class="link"><a href="#">Artists</a></li>
        <li class="link link-dot"><a href="#"><i class="icon-dots-vertical"></i></a></li>
      </ul>
      <a href="{{ route('upload') }}" class="link-button">Upload</a>
      <a href="#" class="link-button">Create</a>
      <a href="#" class="link-login"><i class="icon-login"></i><span>Log In</span></a>
    </div>
    <div id="categories-mobile" class="is-hidden">
      <div class="container">
        <div class="categories-section">
          <h2><a href="#">Categories <i class="icon-right"></i></a></h2>
          <ul>
            <li><a href="#">Artists</a></li>
            <li><a href="#">Reactions</a></li>
            <li><a href="#">Entertainment</a></li>
            <li><a href="#">Sports</a></li>
            <li><a href="#">GIPHY Studios</a></li>
            <li><a href="#">Holiday</a></li>
            <li><a href="#">Animals</a></li>
            <li><a href="#">Emotions</a></li>
          </ul>
        </div>

        <div class="categories-section">
          <h2><a href="#">Stickers <i class="icon-right"></i></a></h2>
          <ul>
            <li><a href="#">GIPHY Studios</a></li>
            <li><a href="#">Trending</a></li>
            <li><a href="#">Reactions</a></li>
            <li><a href="#">Packs</a></li>
          </ul>
        </div>

        <div class="categories-section">
          <h2><a href="#">Apps <i class="icon-right"></i></a></h2>
          <ul>
            <li><a href="#">GIPHY</a></li>
            <li><a href="#">GIPHY World</a></li>
            <li><a href="#">GIPHY Cam</a></li>
            <li><a href="#">GIPHY Keys</a></li>
          </ul>
        </div>

        <div class="categories-section">
          <h2><a href="#">About <i class="icon-right"></i></a></h2>
          <ul>
            <li><a href="#">Terms</a></li>
            <li><a href="#">Privacy</a></li>
            <li><a href="#">DMCA</a></li>
            <li><a href="#">FAQ</a></li>
            <li><a href="#">Engineering Blog</a></li>
            <li><a href="#">Community Guidelines</a></li>
          </ul>
        </div>

        <div class="categories-last-section">
          <h4>© 2019 GIPHY, Inc.</h4>
          <ul>
            <li><a href="#">Terms of Service</a></li>
            <li><a href="#">Community Guidelines</a></li>
            <li><a href="#">Privacy Policy</a></li>
            <li><a href="#">Copyright</a></li>
          </ul>
        </div>

      </div>
    </div>
    <div id="categories-desktop" class="is-hidden">
      <div class="categories-desktop-sections">
        <div class="categories-section">
          <h2><a href="#">Categories</a></h2>
          <ul>
            <li><a href="#">GIPHY Studios</a></li>
            <li><a href="#">Animals</a></li>
            <li><a href="#">Holidays</a></li>
            <li><a href="#">Food & Drink</a></li>
            <li><a href="#">Memes</a></li>
            <li><a href="#">Actions</a></li>
            <li><a href="#">Emotions</a></li>
            <li><a href="#">Anime</a></li>
            <li><a href="#">Gaming</a></li>
            <li><a href="#">Cartoons</a></li>
          </ul>
        </div>
        <div class="categories-section no-columns">
          <h2><a href="#">Stickers</a></h2>
          <ul>
            <li><a href="#">GIPHY Studios</a></li>
            <li><a href="#">Trending</a></li>
            <li><a href="#">Reactions</a></li>
            <li><a href="#">Packs</a></li>
          </ul>
        </div>
        <div class="categories-section no-columns">
          <h2><a href="#">Apps</a></h2>
          <ul>
            <li><a href="#">GIPHY</a></li>
            <li><a href="#">GIPHY World</a></li>
            <li><a href="#">GIPHY Cam</a></li>
            <li><a href="#">GIPHY Capture</a></li>
          </ul>
        </div>
        <div class="categories-section">
          <h2><a href="#">About</a></h2>
          <ul>
            <li><a href="#">Team</a></li>
            <li><a href="#">Engineering Blog</a></li>
            <li><a href="#">Studios</a></li>
            <li><a href="#">Developers</a></li>
            <li><a href="#">Labs</a></li>
            <li><a href="#">FAQ</a></li>
            <li><a href="#">Support</a></li>
            <li><a href="#">Terms</a></li>
            <li><a href="#">Privacy</a></li>
            <li><a href="#">DMCA</a></li>
            <li><a href="#">Guidelines</a></li>
          </ul>
        </div>
      </div>
      <div class="categories-desktop-footer">
        <h6>© 2019 GIPHY, Inc.</h6>
        <ul>
          <li>Terms of Service</li>
          <li>Community Guidelines</li>
          <li>Privacy Policy</li>
          <li>Copyright</li>
        </ul>
      </div>
    </div>
  </div>
</nav>
