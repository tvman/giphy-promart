@extends('layout.app')

@section('nav')
  @include('_includes.nav')
@endsection

@section('search-box')
  @include('_includes.search_box')
@endsection

@section('search-results')
  @include('_includes.search_results')
@endsection

@section('trending')
  @include('_includes.trending')
@endsection

@section('button_use-app')
  <a href="#" target="_blank" id="use-app">Use Our App</a>
@endsection

@section('scripts')

@endsection
