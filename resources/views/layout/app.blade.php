<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Reto Giphy - Promart</title>
  <!-- Style -->
  <link href="{{ asset('css/giphy.css') }}" rel="stylesheet">
</head>
<body>
  <div id="loader">
    <div class="wrap-loader">
      <div class="dot dot-a"></div>
      <div class="dot dot-b"></div>
      <div class="dot dot-c"></div>
      <div class="dot dot-d"></div>
    </div>
  </div>
  @yield('nav')
  <div id="app">
    @yield('search-box')
    <main>
      <div class="container">
      @yield('search-results')
      @yield('trending')
      @yield('upload')
      </div>
    </main>
    @yield('button_use-app')
  </div>

  <!-- Scripts -->
  <script src="{{ asset('js/app.js') }}"></script>
  @yield('scripts')
</body>
</html>
