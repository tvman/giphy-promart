@extends('layout.app')

@section('upload')
<section id="upload-mobile">
  <div class="wrap-upload-mobile">
    <h1>Gift Maker</h1>
    <h2>Upload & Edit GIFs</h2>
    <div class="upload-mobile-buttons">
      <input type="file" class="upload-camera">
      <input type="url" class="upload-url" placeholder="Paste an image or video URL">
    </div>
  </div>
  <footer class="upload-mobile-footer">
    <h4><a href="#">Log in</a> to save GIF to your channel page.</h4>
    <ul>
      <li><a href="#">Community Guidlines</a></li>
      <li><a href="#">Privacy Policy</a></li>
    </ul>
  </footer>
</section>

<section id="upload-desktop">
  <nav id="nav" class="upload-desktop-nav">
      <a href="{{ route('index') }}" class="logo">
        <div class="logo-svg">
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 28 35" itemProp="logo">
            <g fill-rule="evenodd" clip-rule="evenodd">
            <path fill="#00ff99" d="M0 3h4v29H0z"></path>
            <path fill="#9933ff" d="M24 11h4v21h-4z"></path>
            <path fill="#00ccff" d="M0 31h28v4H0z"></path>
            <path fill="#fff35c" d="M0 0h16v4H0z"></path>
            <path fill="#ff6666" d="M24 8V4h-4V0h-4v12h12V8"></path>
            <path class="shadow" d="M24 16v-4h4M16 0v4h-4"></path></g>
          </svg>
        </div>
        <header>
          <h1 class="logo-text">Giphy</h1>
        </header>
      </a>
      <a href="#" class="upload-desktop-login"><i class="icon-login"></i><span>Log In</span></a>
  </nav>
  <div class="wrap-upload-desktop">
    <header>
      <h3>UPLOAD</h3>
      <p>Upload your GIF collection to share on Facebook, Twitter, Instagram, text message, email, and everywhere else.</p>
    </header>
    <div class="upload-desktop-inputs">
      <div class="input-first">
        <div class="input-first-left">
          <div class="upload-pc">
            <div class="upload-pc-icon"></div>
            <p>Browse Your Files <br><span>Upload one or more GIFs</span></p>
          </div>
        </div>
        <div class="input-first-right">
          <div class="upload-dragdrop">
            <div class="upload-dragdrop-icon"></div>
            <p>Drag and drop GIFs <br><span>Drop them right here, right now.</span></p>
          </div>
        </div>
      </div>
      <div class="input-last">
        <p>Copy and paste any gif url</p>
        <input type="url" placeholder="Copy and paste video, GIF, or image URL">
      </div>
    </div>
    <div class="upload-desktop-links">
      <a href="#">Community Guidelines</a>
      <a href="#">Privacy Policy</a>
    </div>
  </div>
</section>

@endsection
