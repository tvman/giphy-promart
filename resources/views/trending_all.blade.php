@extends('layout.app')

@section('nav')
  @include('_includes.nav')
@endsection

@section('search-box')
  @include('_includes.search_box')
@endsection

@section('search-results')
  @include('_includes.search_results')
@endsection

@section('trending')
  <section id="trending">
    <header>
      <h2>Trending Gifs</h2>
    </header>
    <ul>
      <li v-if="trendingsAll" v-for="gif in trendingsAll">
        <img v-lazy="gif.images.original.webp">
      </li>
    </ul>
  </section>
@endsection
