
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/* Lazyload */
import VueLazyload from 'vue-lazyload'

Vue.use(VueLazyload, {
  preLoad: 1,
  attempt: 1
})

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('example-component', require('./components/ExampleComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
  el: '#app',

  data: {
    apiKey: 'dc6zaTOxFJmzC',
    apiURL: 'http://api.giphy.com/v1/gifs',
    offset: 25,
    trendings: [],
    trendingsAll: [],
    searchs: [],
    keyword: '',
    totalCount: null
  },

  methods: {

    // trendingGifs(){
    //   const url = `${this.apiURL}/trending?api_key=${this.apiKey}&limit=5`;
    //   fetch(url)
    //         .then(response => response.json())
    //         .then(data => this.gifs = data.data);
    // },

    getTrendingGifs(){
      var _this = this;
      axios.get(_this.apiURL + '/trending?', {
        params:{
          key: _this.apiKey,
          limit: 20,
          offset: _this.offset
        }
      })
      .then(function (response){
        _this.trendings = _this.trendings.concat(response.data.data);
        console.log(response.data.data);
      })
      .catch(function (error){
        console.log(error);
      });

    },

    getTrendingGifsAll(){
      var _this = this;
      axios.get(_this.apiURL + '/trending?', {
        params:{
          key: _this.apiKey,
          limit: 25,
          offset: _this.offset
        }
      })
      .then(function (response){
        _this.trendingsAll = _this.trendingsAll.concat(response.data.data);
        _this.offset = _this.offset + response.data.data.length;
        console.log(response.data.data);
      })
      .catch(function (error){
        console.log(error);
      });

    },

    getSearchGifs(){
      var _this = this;
      axios.get(_this.apiURL + '/search?q=' + _this.keyword, {
        params:{
          key: _this.apiKey
        }
      })
      .then(function (response){
        _this.searchs = response.data.data;
        _this.totalCount = response.data.pagination.total_count;
        console.log(response);
      })
      .catch(function (error){
        console.log(error);
      });
    },

    scroll (trendings) {
      window.onscroll = () => {
      let bottomOfWindow = document.documentElement.scrollTop + window.innerHeight === document.documentElement.offsetHeight;

      if (bottomOfWindow) {
          this.getTrendingGifsAll();
        }
      };
    },

  },

  created() {
    // this.trendingGifs();
    this.getTrendingGifs();
    this.getTrendingGifsAll();
  },

  mounted() {
    this.scroll(this.trendings);
  },

});

$(document).ready(function() {
  /* Loader */
  $(window).on("load", function(){
     $('#loader').fadeOut();
  });

  /* Animación Logo SVG (svg-effect) */
  setInterval(function(){
      $('.logo-svg').toggleClass('svg-effect');
  },3000);

  /* Navbar*/
  $('.link-bar').on("click", function() {
    $('#categories-mobile').toggleClass('is-hidden');
    $('.link-bar svg').toggleClass('bar-off');
  });
  $('.link-dot').hover(function() {
    $('#categories-desktop').toggleClass('is-hidden');
  });

  /* Nav Scroll */
  $(window).scroll(function() {
    var e = $(window).scrollTop(),
        o = $("#search, .logo");
    56 < e ? o.addClass("nav-scroll") : o.removeClass("nav-scroll")
  });

  /* Input Search Box */
  $('.search-input').on("keyup blur", function(e) {
    if($(e.target).val().length > 0){
      $('.text-animations').addClass('is-hidden');
      $('#search-results').removeClass('is-hidden');
    } else {
      $('.text-animations').removeClass('is-hidden');
      $('#search-results').addClass('is-hidden');
    }
  });

})
